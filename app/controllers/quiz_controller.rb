class QuizController < ApplicationController
  skip_before_filter :verify_authenticity_token

  def index

  end

  def create
    @quiz = Quiz.create
    @quantity_right_answers = 0
    # render plain: params.inspect
    answers_hash = params
    answers_arr = answers_hash.values
    answers_arr -= ["quiz", "create"]

    result = compare_with_right(answers_arr)
    @quantity_right_answers = result.length
    result = arr_to_s(result)

    @quiz.answers = result
    @quiz.save!

    render action: "show"
  end

  def new
  end

  def show
  end

  def not_found
    render(:status => 404)
  end

private
  def compare_with_right(answers_arr)
    right_answers = ["1c", "2c", "3b", "4c", "5a", "6a", "7b", "8a", "9a", "10b"]
    result = answers_arr & right_answers
  end

  def arr_to_s(arr)
    s = arr*','
  end

  def s_to_arr(s)
    arr = s.split(',')
  end
end
