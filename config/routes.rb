Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :quiz, only: [:index, :create, :new, :show, :not_found]
  root 'quiz#index'
end
